---
title: 'Notes'
date: 2019-02-11T19:30:08+10:00
draft: false
weight: 1
---

## Notes  and links
-----

### Node.js & JavaScript Testing Best Practices (2019)

https://medium.com/@me_37286/yoni-goldberg-javascript-nodejs-testing-best-practices-2b98924c9347

### How to Unit Test Private Functions in JavaScript

https://philipwalton.com/articles/how-to-unit-test-private-functions-in-javascript/

### Understanding passport.js authentication flow

http://toon.io/understanding-passportjs-authentication-flow/ 


### GOTO 2018 • INTRODUCTION TO OAUTH 2.0 AND OPENID CONNECT <BR/><BR/> PHILIPPE DE RYCK
Philippe De Ryck - Founder of Pragmatic Web Security, Google Developer Expert

📓 [Slides PDF](https://files.gotocon.com/uploads/slides/conference_12/653/original/20181101%20-%20Introduction%20to%20OAuth%202.0%20and%20OpenID%20Connect.pdf)    📹[Youtube](https://www.youtube.com/watch?v=GyCL8AJUhww)

<iframe width="560" height="315" src="https://www.youtube.com/embed/GyCL8AJUhww" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---
### NODEJS BEST PRACTICES

A repo with nodejs best practices. Currently, more than 80 best practices, style guides, and architectural tips are presented. 
[https://github.com/i0natan/nodebestpractices]

### BRANCHING
Comparing Workflows
Atlassian
[https://www.atlassian.com/git/tutorials/comparing-workflows]

### Planning and Estimating 
<iframe width="560" height="315" src="https://www.youtube.com/embed/37zfyncCpkA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/D2r2KryYAaY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/8iBW3nejTKM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

