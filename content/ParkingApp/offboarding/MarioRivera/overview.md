+++
title= "Overview"
date= 2019-05-03T13:26:55-05:00
description = "I mainly was working on the authentication Oauth workflow and implementing a centralize body of knowledge for Parking App"
draft= false
author="Mario<@mrivera>"
tags = ["Authetication", "Management"]
weight=1
+++

## Overview
I mainly was working on the authentication Oauth workflow and implementing a centralize body of knowledge for Parking App

## What was I working on ?

* Authentication Workflow with OAuth2.0
* Implementation of HUGO framework page 
* Hosting of HUGO pages in Gitlab repository
* Guide for migrating the Hugo page to a group respository

## References
* [Hugo Framework](https://gohugo.io/about/)
* [How To Password Protect A Hugo Site](https://www.aerobatic.com/blog/password-protect-a-hugo-site/)
* [Hugo - Static Site Generator | Tutorial {{< icon name="fa-youtube-play" size="large" >}}](https://www.youtube.com/playlist?list=PLLAZ4kZ9dFpOnyRlyS-liKL5ReHDcj4G3)
* [Hugo migration Guide](/migration_guide)