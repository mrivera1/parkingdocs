+++
title= "Hugo Pages : Migration Guide & Management"
date= 2019-05-03T14:34:08-05:00
description = "Hugo is a framework for generating static pages, which are very suitable for documentations hosted as GitLab pages"
draft= false
author="Mario<@mrivera>"
tags = ["Management"]
+++

Hugo is a framework for generating static **HTML** pages from MARKDOWN FILES(**.md**), which are very suitable for documentations hosted as GitLab pages.
In the next sections will cover how to migrate an existing hugo project and host it into a new repository that belongs to a group.(e.g **https://gitlab.com/tiempodevce**)

---
## Summary


* [Installation]({{< permalink >}}#Installation)
* [Getting Started]({{< permalink >}}#getting-started)
* [Configuration]({{< permalink >}}#configuration)
* [Adding Content]({{< permalink >}}#adding-content)
* Style Guide
    * Suggested Sections

<a name="Installation"></a>
### Installation
#### Step 1: Install Hugo
##### Installation in Mac

{{% panel %}}Homebrew, a package manager for macOS, can be installed from brew.sh. See install if you are running Windows etc.{{% /panel %}}

```bash
brew install hugo
```
To verify your new install:
```bash
hugo version
```

##### Installation in Windows

**Chocolatey (Windows)**

If you are on a Windows machine and use Chocolatey for package management, you can install Hugo with the following one-liner:

install-with-chocolatey.ps1
```
choco install hugo -confirm
```
**Scoop (Windows)**

If you are on a Windows machine and use Scoop for package management, you can install Hugo with the following one-liner:

``` 
scoop install hugo
```
{{% panel  header="For more  detailed information see"%}}
( <a href="https://gohugo.io/getting-started/installing">Install Hugo</a> )
{{% /panel %}}

<a name="getting-started"></a>
## Getting Started

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
2. Install Hugo
3. Preview your project: hugo server
4. Modify configuration
5. Add content

### GitLab User or Group Pages
To use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings**.

You'll need to configure your site too: change this line
in your `config.toml`, from `"https://pages.gitlab.io/hugo/"` to `baseurl = "https://namespace.gitlab.io"`.
Proceed equally if you are using a [custom domain](https://about.gitlab.com/2016/04/07/gitlab-pages-setup/#custom-domains): `baseurl = "http(s)://example.com"`. Read more about [user/group Pages](https://about.gitlab.com/2016/04/07/gitlab-pages-setup/#custom-domains) and [project Pages](http://doc.gitlab.com/ee/pages/README.html#project-pages).

### Did you fork this project?
If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

<a name="configuration"></a>
## Configuration
In order to modify the configuration and make the project works on other repository you'll need to modify the `config.toml` file replacing  NAMESPACE for  your own and  PROJECT-NAME with the name of the project, In the case that you want to change the name of your project/repository  . (e.g Let's say  that the url of the repository in which we are going to host the project looks like this: `https://gitlab.com/tiempodevce/TiempoparkingAppDocs` then you should change the `baseURL`  like this: 


{{< highlight toml "linenos=inline, hl_lines=1-2" >}}

# baseURL = "https://{NAMESPACE}.gitlab.io/{PROJECT-NAME}"
baseURL = "https://tiempodevce.gitlab.io/TiempoparkingAppDocs"

languageCode = "en-us"
DefaultContentLanguage = "en"
title = "Pool Projects"
theme = "docdock"
# themesdir = "../.."

...
{{< /highlight >}}
<a name="adding-content"></a>
## Adding Content

We'll need to use the Hugo CLI  `new` command  in order to add content to our site, let's say we want to add a page for **overview** of the  **mvp** section from **ParkingApp**, then we'll use the new command like in the next example

```
hugo new ParkingApp/mvp/overview.md
```
This  command will add the **overview.md**  file  like this : 

``` md
+++
title= "Overview"
date= 2019-05-04T22:11:57-05:00
description = ""
draft= false
author="Mario<@mrivera>"
tags=["Develop"] 
+++

Lorem Ipsum.
Notice `draft` is set to true.

```
As we can see it's a markdown file, with an initial section containing some metadata for the page known as [**Front Matter**](https://gohugo.io/content-management/front-matter/) that can be used by the site's theme 

For a more comprehensive guide about markdown see next links:

* https://gohugo.io/content-management/formats/
* https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet

### Shortcodes  
Additional to the markdown syntax there are  some special codes that can be used by the hugo framework known as __shortcode__ that can enrich the content of the page.

Built-in  hugo framework shortcodes(https://gohugo.io/content-management/shortcodes/)

and other that are specific to the theme in use, 
**DocDock** theme shortcodes ( https://docdock.netlify.com/shortcodes/)

## Style Guide
The next  section is a compilation of recommendations about the use and 