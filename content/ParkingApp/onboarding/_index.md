+++
title= "Onboarding"
date= 2019-05-02T02:02:07-05:00
description = ""
draft= false
Weight=1
+++

---

<a name="index"></a>    

## Index
- [Introduction](#introduction)
- [Setting up your Environment](#environment)
- [Tools](#tech-stack)
- [Offboarding](#tech-stack)

---

<a name="Introduction"></a>
## Introduction  
Welcome, in this section you will find the tools and configurations you'll need throughout the project.

---
<a name="environment"></a>
## Setting up your environment

First of all set up your ssh key in order to config your gitlab account 
 
### Generating a  new SSH key 

1. Open a Terminal(macOS/Linux based) or [Git bash](https://git-scm.com/downloads)(for Windows users)
2. Copy and paste the text below, substituting in your GitLab email address ( preferably your **@tiempodevelopment** account)

    ```bash 
    $ ssh-keygen -t rsa -b 4096 -C "your_email@example.com"
    ```
3. This creates a new ssh key, using the provided email as a label.

    ```bash 
    Generating public/private rsa  key pair 
    ```
4. When you are prompted to "Enter a file  in which  to save  the key, " press ```ENTER```. This  accepts the default file location.
    ```bash 
    Enter a file  in which  to save  the key(/c/Users/you/.ssh/id_rsa): [Press Enter]
    ```
5. At the prompt, type  a secure passphrase. 
    ```bash 
    Enter passphrase (empty for no passphrase) : [Type a passphrase]
    Enter same passphrase again: [Type a passphrase]
    ```
6. Since you have Git Bash open, run: cat .ssh/id_rsa.pub. Copy to the clipboard from ssh-rsa into your email

### Git Lab Configuration
1. Login into https://gitlab.com/ and go to https://gitlab.com/profile/keys
    
    ![gitlabConfig]({{< baseurl>}}/img/onboarding/sshkeyGitLab.png)

2. Install Docker CE https://www.docker.com/community-edition#/download
3. Install Node https://nodejs.org/en/
4. Open the terminal/Git Bash console
    ```bash
    cd cetiempotool
    git clone git@gitlab.com:tiempodevce/tiempofront.git client
    cd client
    npm install
    cd ..
    git clone git@gitlab.com:tiempodevce/tiempoback.git tiempo-api
    docker network create tiempo-ce-network
    docker-compose build
    ```

    {{% panel theme="info" header="Note" %}}If you are using Windows: Open a PowerShell window with Administrator rights and run the following command: Set-NetConnectionProfile -interfacealias "vEthernet (DockerNAT)" -NetworkCategory Private 
        <ul>
        <li> Open Docker Settings Window from the bottom right arrow</li>
        <li>Go to Shared Drives and check “C” Drive and click on “Apply” button</li>
        </ul>
    
    {{% /panel %}}
    

7. Open a **new terminal/Git Bash console** and run: ```docker-compose up mysqldb```
8. Wait until mysql container is up 
9. Open a **new terminal/Git Bash console** and run: ```docker-compose up tiempo-api```
10. Open a **new terminal/Git Bash console** and run: ```docker-compose up app```
11. When everything is up and running you can go to http://localhost:3005/ . This is the front-end web app
