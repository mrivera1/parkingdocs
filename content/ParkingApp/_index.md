+++
title= "Parking App"
date= 2019-05-02T01:01:40-05:00
description = ""
draft= false
tags=["Parkingapp", "Projects"]

+++
<a name="introduction"></a>

## Introduction 
**Parking app** is the brand new project developed by the engineers in the pool department.
The main intention for this project is to develop a tool that enable the workers from **Tiempo Development** to get access to the parking spots from the respective sites (Chapalita, Justo Sierra, Hermosillo, Monterrey) select the ones for their vehicles, as well as notify if there's a car blocking their way. 

---

<a name="index"></a>

## Index
- [Introduction](#introduction)
- [Technology Stack](#tech-stack)
- [Goals](#goals)
- [Next Steps](#next-steps)

---

<a name="tech-stack"></a>

### Technology Stack 

|**Backend**    | **Frontend**  | **Database**  |
|-----------    |--------       |---            |
|JavaScript     |JavaScript     | MySQL         |
|Nodejs         |React          |               |
|Express        |ReactNative    |               |
|Oauth2         |-              |               |
|REST Services  |-              |               |
|Google API     |-              |               |
|JWT            |-              |               |
|Passport       |-              |               |
|Sequialize     |-              |               |

[[Top]{{< icon name="fa-arrow-up" size="large" >}} ](#introduction)

<a name="goals"></a>
## Goals
* Web App
 - [ ] All users information (admin panel)
 - [ ] Logs access reports.
 - [ ] Show parking status
 - [ ] Notify to the user if left the parking without uncheck the slot
 - [ ] APIs (4 Mobile)

* Mobile App
 - [ ] Show the available parking slots
 - [ ] Mark (send request) ‘the slot no available’
 - [ ] Send a notification if some car is blocking
 - [ ] Update of uncheck the slot when car is moved
 - [ ] Confidential of the other cars

* Authentication 
 - [ ] Authentication with Code Grant Workflow 
 - [ ] Authentication with Implicit Workflow 
 - [ ] Authentication with Implicit Workflow 

[[Top]{{< icon name="fa-arrow-up" size="large" >}} ](#introduction)

<a name="next-steps"></a>
### Next Steps
* [Onboarding](onboarding) 
* [Definition of MVP](mvp)
* [Architecture Document](architecture)

