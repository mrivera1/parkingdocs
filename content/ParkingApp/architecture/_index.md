+++
title= "Architecture"
date= 2019-05-02T02:07:58-05:00
description = ""
draft= false
Weight=3
tags=["Architecture", "Development"]
+++

## Overview
![diagram]({{< baseurl>}}/flow_process_diagram.jpg?classes=border,shadow)

## Entities
* Slots
* Sites/Location
* Users
* cars
* authentication
* socket.io endpoints



## Endpoints


GET /slots
